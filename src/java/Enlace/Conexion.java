package Enlace;

import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author clau
 */
public class Conexion {
     //Creo el objeto para conectar
    Connection cnx=null;
   //Creo las variables de
    private String bd="acta";
    private String urlbd="jdbc:mysql://localhost:3306/"+bd;
    private String userbd="root";
    private String passbd="";
    //constructor

    public Conexion() {
        try{
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            cnx=(Connection) DriverManager.getConnection(urlbd, userbd, passbd);//Conecto la bd
        }catch(Exception e){
            System.out.println("Conexion"+e);
        }
    }
    //retorno la conecion
    public Connection obtenerConexion(){
        return cnx;//Retorno la conexion
    }
}
