
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlBean;

import dao.daoUsuario;
import java.util.ArrayList;
import java.util.List;
import modelo.Usuario;

/**
 *
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */

public class controlUsuario {

    /**
     * Creates a new instance of controlUsuario
     */
    
    private Usuario us;
    private daoUsuario dus;
    private List<Usuario> listUs;
    
    public controlUsuario() {
        us=new Usuario();
        dus=new daoUsuario();
        listUs=new ArrayList<>();
        this.Listar();
               
    }

    public Usuario getUs() {
        return us;
    }

    public void setUs(Usuario us) {
        this.us = us;
    }

    public daoUsuario getDus() {
        return dus;
    }

    public void setDus(daoUsuario dus) {
        this.dus = dus;
    }

    public List<Usuario> getListUs() {
        return listUs;
    }

    public void setListUs(List<Usuario> listUs) {
        this.listUs = listUs;
    }
    private void Listar(){
        this.listUs=dus.getListaUsuarios();
    }
    public String registro(){
        String retorno="Error";
        try{
            if(dus.registroUsuario(this.us)>0){
                this.Listar();
                retorno= "Registro";
            }
           
        }catch(Exception e){
            System.out.println("eeeeee"+e);
            
        }
      return  retorno;
    }
}
