/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlBean;

import dao.daoAprendiz;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import modelo.Aprendiz;

/**
 *
 * @author casa
 */
@ManagedBean
@RequestScoped
public class controlAprendiz {

    /**
     * Creates a new instance of controlAprendi
     */
     private Aprendiz us;
    private daoAprendiz dus;
    private List<Aprendiz> listUs;
    
    public controlAprendiz() {
        us=new Aprendiz();
        dus=new daoAprendiz();
        listUs=new ArrayList<>();
        this.Listar();
    }
  

    public Aprendiz getUs() {
        return us;
    }

    public void setUs(Aprendiz us) {
        this.us = us;
    }

    public daoAprendiz getDus() {
        return dus;
    }

    public void setDus(daoAprendiz dus) {
        this.dus = dus;
    }

    public List<Aprendiz> getListUs() {
        return listUs;
    }

    public void setListUs(List<Aprendiz> listUs) {
        this.listUs = listUs;
    }
    
    private void Listar(){
        this.listUs=dus.getListaAprendiz();
    }
    public String registro(){
        String retorno="Error";
        try{
            if(dus.registroAprendiz(this.us)>0){
                this.Listar();
                retorno= "Registro";
            }
           
        }catch(Exception e){
            System.out.println("eeeeee"+e);
            
        }
      return  retorno;
    }
}