/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author casa
 */
public class Aprendiz {
 
    //Se declaran los atributos
    private int identificacion;
    private String nombre_Aprendiz;
    private String apellido_Aprendiz;
    private int telefono;
    private int celular;
    private String direccion;
    private String correo;
    
    private int fkTipoId;
    private int fkFicha;
    private int fkColegio;
    private int fkEntidad;
    private int fkAprendiz;
    private int estado_Aprendiz;

    //Constructor por defecto
    public Aprendiz() {
    }

    //Constructor sobre cargado
    public Aprendiz(int identificacion, String nombre_Aprendiz, String apellido_Aprendiz, int telefono, int celular, String direccion, String correo, int fkTipoId, int fkFicha, int fkColegio, int fkEntidad, int fkAprendiz, int estado_Aprendiz) {
        this.identificacion = identificacion;
        this.nombre_Aprendiz = nombre_Aprendiz;
        this.apellido_Aprendiz = apellido_Aprendiz;
        this.telefono = telefono;
        this.celular = celular;
        this.direccion = direccion;
        this.correo = correo;
        this.fkTipoId = fkTipoId;
        this.fkFicha = fkFicha;
        this.fkColegio = fkColegio;
        this.fkEntidad = fkEntidad;
        this.fkAprendiz = fkAprendiz;
        this.estado_Aprendiz = estado_Aprendiz;
    }

    //Encapsulamiento
    public int getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(int identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombre_Aprendiz() {
        return nombre_Aprendiz;
    }

    public void setNombre_Aprendiz(String nombre_Aprendiz) {
        this.nombre_Aprendiz = nombre_Aprendiz;
    }

    public String getApellido_Aprendiz() {
        return apellido_Aprendiz;
    }

    public void setApellido_Aprendiz(String apellido_Aprendiz) {
        this.apellido_Aprendiz = apellido_Aprendiz;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public int getCelular() {
        return celular;
    }

    public void setCelular(int celular) {
        this.celular = celular;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public int getFkTipoId() {
        return fkTipoId;
    }

    public void setFkTipoId(int fkTipoId) {
        this.fkTipoId = fkTipoId;
    }

    public int getFkFicha() {
        return fkFicha;
    }

    public void setFkFicha(int fkFicha) {
        this.fkFicha = fkFicha;
    }

    public int getFkColegio() {
        return fkColegio;
    }

    public void setFkColegio(int fkColegio) {
        this.fkColegio = fkColegio;
    }

    public int getFkEntidad() {
        return fkEntidad;
    }

    public void setFkEntidad(int fkEntidad) {
        this.fkEntidad = fkEntidad;
    }

    public int getFkAprendiz() {
        return fkAprendiz;
    }

    public void setFkAprendiz(int fkAprendiz) {
        this.fkAprendiz = fkAprendiz;
    }

    public int getEstado_Aprendiz() {
        return estado_Aprendiz;
    }

    public void setEstado_Aprendiz(int estado_Aprendiz) {
        this.estado_Aprendiz = estado_Aprendiz;
    }

    
    
}
