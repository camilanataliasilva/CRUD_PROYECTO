/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dao;

import Enlace.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import modelo.Usuario;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
public class daoUsuario extends Conexion  {
    private Statement st;
    private ResultSet rs;
    private Usuario usuario;
    private List<Usuario> listaUsuarios;

    public daoUsuario() {
          this.listaUsuarios=new ArrayList<>();
          try{
              this.llenarListUsuario();
          }catch(Exception e){
              System.out.println("daoUsuario exception :"+e);
          }
        
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public List<Usuario> getListaUsuarios() {
        return listaUsuarios;
    }

    public void setListaUsuarios(List<Usuario> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }
    private void llenarListUsuario()throws SQLException {
        if(this.obtenerConexion()!=null){
            st=this.obtenerConexion().createStatement();
            rs=st.executeQuery("Select * from Usuario");
            while(rs.next()){
              this.usuario=new Usuario();
              this.usuario.setIdUsuario(rs.getInt("IdUsuario"));
              this.usuario.setNombreUsuario(rs.getString("NombreUsuario"));
              this.usuario.setContraseña(rs.getString("Contraseña"));
              this.listaUsuarios.add(usuario);
              
            }
            
        }
    }
    public int registroUsuario(Usuario us)throws SQLException {
        int v=0;
        if(this.obtenerConexion()!=null){
            st=this.obtenerConexion().createStatement();
            String query="insert into Usuario values(null,'"+us.getNombreUsuario()+"','"+us.getContraseña()+"')";
             v=st.executeUpdate(query);       
        }
        return v;   
    }  
}
