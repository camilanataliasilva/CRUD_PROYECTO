/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import Enlace.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import modelo.Aprendiz;

/**
 *
 * @author casa
 */
public class daoAprendiz extends Conexion{
   
    private Statement st;
    private ResultSet rs;
    private Aprendiz Aprendiz;
    private List<Aprendiz> listaAprendiz;

    public daoAprendiz() {
          this.listaAprendiz=new ArrayList<>();
          try{
              this.llenarListUsuario();
          }catch(Exception e){
              System.out.println("daoUsuario exception :"+e);
          }
        
    }


    
    private void llenarListUsuario()throws SQLException {
        if(this.obtenerConexion()!=null){
            st=this.obtenerConexion().createStatement();
            rs=st.executeQuery("Select * from Usuario");
            while(rs.next()){
              this.Aprendiz=new Aprendiz();
              this.Aprendiz.setIdentificacion(rs.getInt("identificacion"));
              this.Aprendiz.setNombre_Aprendiz(rs.getString("nombre_Aprendiz"));
              this.Aprendiz.setApellido_Aprendiz(rs.getString("aprellido_Aprendiz"));
              this.Aprendiz.setTelefono(rs.getInt("telefono"));
              this.Aprendiz.setCelular(rs.getInt("celular"));
              this.Aprendiz.setDireccion(rs.getString("direccion"));
              this.Aprendiz.setCorreo(rs.getString("correo"));
              this.Aprendiz.setFkTipoId(rs.getInt("fkTipoId"));
              this.Aprendiz.setFkFicha(rs.getInt("fkFicha"));
              this.Aprendiz.setFkColegio(rs.getInt("fkColegio"));
              this.Aprendiz.setFkEntidad(rs.getInt("fkEntidad"));
              this.Aprendiz.setFkAprendiz(rs.getInt("fkAprendiz"));
              
              
              this.listaAprendiz.add(Aprendiz);
              
            }
            
        }
    }
    public int registroAprendiz(Aprendiz us)throws SQLException {
        int v=0;
        if(this.obtenerConexion()!=null){
            st=this.obtenerConexion().createStatement();
            String query="insert into Aprendiz values(null,'"+us.getNombre_Aprendiz()+"','"+us.getApellido_Aprendiz()+"',"+us.getTelefono()+","+us.getCelular()+",'"+us.getDireccion()+"','"+us.getCorreo()+"',"+us.getFkTipoId()+","+us.getFkFicha()+","+us.getFkColegio()+","+us.getFkEntidad()+","+us.getFkAprendiz()+")";
             v=st.executeUpdate(query);       
        }
        return v;   
    }  

    public Statement getSt() {
        return st;
    }

    public void setSt(Statement st) {
        this.st = st;
    }

    public ResultSet getRs() {
        return rs;
    }

    public void setRs(ResultSet rs) {
        this.rs = rs;
    }

    public Aprendiz getAprendiz() {
        return Aprendiz;
    }

    public void setAprendiz(Aprendiz Aprendiz) {
        this.Aprendiz = Aprendiz;
    }

    public List<Aprendiz> getListaAprendiz() {
        return listaAprendiz;
    }

    public void setListaAprendiz(List<Aprendiz> listaAprendiz) {
        this.listaAprendiz = listaAprendiz;
    }
}

